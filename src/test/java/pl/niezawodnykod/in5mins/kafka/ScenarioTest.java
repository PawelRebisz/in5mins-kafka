package pl.niezawodnykod.in5mins.kafka;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Testcontainers
class ScenarioTest {

    public static final String TOPIC = "topic-1";

    @Container
    private final KafkaContainer container = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:5.4.3"));

    @BeforeEach
    void beforeEach() {
        assertTrue(container.isRunning());
    }

    @Test
    void consumerShouldFetchEventsFromMultiplePartitionsOutOfOrder() {
        // given
        String bootstrapServers = container.getBootstrapServers();
        createTopic(bootstrapServers, TOPIC, 3);
        try (EventPublisher producer = new EventPublisher(bootstrapServers)) {
            IntStream.range(1, 11)
                    .forEach(counter -> producer.send(TOPIC, "key" + (counter % 3), "msg:" + counter));
        }

        // when
        EventConsumer consumer = new EventConsumer("consumer-1", bootstrapServers);
        consumer.subscribe(TOPIC);

        // then
        await()
            .atMost(10, TimeUnit.SECONDS)
            .untilAsserted(() -> {
                List<String> messages = consumer.getAllEvents();
                assertThat(messages)
                    .containsExactlyInAnyOrder(
                        "msg:1","msg:2","msg:3","msg:4","msg:5",
                        "msg:6","msg:7","msg:8","msg:9","msg:10"
                    );
            });

        // cleanup
        consumer.close();
    }

    @Test
    void multipleConsumerShouldFetchEventsFromMultiplePartitions() {
        // given
        String bootstrapServers = container.getBootstrapServers();
        createTopic(bootstrapServers, TOPIC, 3);
        try (EventPublisher producer = new EventPublisher(bootstrapServers)) {
            IntStream.range(1, 11)
                    .forEach(counter -> producer.send(TOPIC, "key" + (counter % 3), "msg:" + counter));
        }

        // when
        List<EventConsumer> consumers = IntStream.range(1, 3)
                .mapToObj(operand -> {
                    EventConsumer consumer = new EventConsumer("consumer-" + operand, bootstrapServers);
                    consumer.subscribe(TOPIC);
                    return consumer;
                })
                .collect(toList());

        // then
        await()
            .atMost(10, TimeUnit.SECONDS)
            .untilAsserted(() -> {
                List<String> messages = consumers.stream()
                        .map(EventConsumer::getMessages)
                        .flatMap(Collection::stream)
                        .collect(toList());
                assertThat(messages)
                    .containsExactlyInAnyOrder(
                        "msg:1","msg:2","msg:3","msg:4","msg:5",
                        "msg:6","msg:7","msg:8","msg:9","msg:10"
                    );
            });

        // cleanup
        consumers.forEach(EventConsumer::close);
    }

    @Test
    void consumerShouldFetchEventsFromOnePartitionInOrder() {
        // given
        String bootstrapServers = container.getBootstrapServers();
        createTopic(bootstrapServers, TOPIC, 1);
        try (EventPublisher producer = new EventPublisher(bootstrapServers)) {
            IntStream.range(1, 11)
                    .forEach(counter -> producer.send(TOPIC, "key" + (counter % 3), "msg:" + counter));
        }

        // when
        EventConsumer consumer = new EventConsumer("consumer-1", bootstrapServers);
        consumer.subscribe(TOPIC);

        // then
        await()
            .atMost(10, TimeUnit.SECONDS)
            .untilAsserted(() -> {
                List<String> messages = consumer.getAllEvents();
                assertThat(messages)
                    .containsExactly(
                        "msg:1","msg:2","msg:3","msg:4","msg:5",
                        "msg:6","msg:7","msg:8","msg:9","msg:10"
                    );
            });

        // cleanup
        consumer.close();
    }

    static void createTopic(String bootstrapServers, String topicName, int numberOfPartitions) {
        try (AdminClient client = AdminClient.create(PropertiesProvider.adminProperties(bootstrapServers))) {
            NewTopic newTopic = new NewTopic(topicName, numberOfPartitions, (short) 1);
            client.createTopics(Arrays.asList(newTopic));
        }
    }
}
