package pl.niezawodnykod.in5mins.kafka;

import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.io.Closeable;
import java.time.Duration;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
class EventConsumer implements Closeable {

    private final int timeoutMilliSecs = 10;
    private volatile boolean keepItRunning = true;

    @Getter
    private final List<String> messages;
    private final String name;
    private final KafkaConsumer<String, String> consumer;
    private final ExecutorService executorService;

    public EventConsumer(String name, String bootstrapServers) {
        this.name = name;
        this.consumer = new KafkaConsumer<>(PropertiesProvider.consumerProperties(bootstrapServers));
        this.messages = new LinkedList<>();
        this.executorService = Executors.newFixedThreadPool(1);
    }

    @Override
    public void close() {
        keepItRunning = false;
        try {
            executorService.awaitTermination(2, TimeUnit.SECONDS);
        } catch (InterruptedException exception) {
            throw new RuntimeException("Failed to terminate executor.", exception);
        }
        consumer.close();
    }

    public void subscribe(String topic) {
        consumer.subscribe(Arrays.asList(topic));
        executorService.execute(() -> {
            while (keepItRunning) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(timeoutMilliSecs));
                for (ConsumerRecord<String, String> record : records) {
                    log.info("< {} < {}|{} - {}", name, record.topic(), record.key(), record.value());
                    messages.add(record.value());
                }
            }
        });
    }

    public List<String> getAllEvents() {
        return new LinkedList<>(messages);
    }
}
