package pl.niezawodnykod.in5mins.kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.Closeable;
import java.util.Properties;

@Slf4j
class EventPublisher implements Closeable {

    private final KafkaProducer<String, String> producer;

    public EventPublisher(String bootstrapServers) {
        this.producer = new KafkaProducer<>(PropertiesProvider.producerProperties(bootstrapServers));
    }

    @Override
    public void close() {
        producer.close();
    }

    public void send(String topic, String key, String message) {
        ProducerRecord<String, String> record = new ProducerRecord<>(topic, key, message);
        log.info(">> {}|{} - {}", topic, key, message);
        producer.send(record);
    }
}
